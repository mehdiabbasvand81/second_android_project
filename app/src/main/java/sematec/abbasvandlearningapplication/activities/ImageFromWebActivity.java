package sematec.abbasvandlearningapplication.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import sematec.abbasvandlearningapplication.R;

public class ImageFromWebActivity extends AppCompatActivity {
    ImageView img;
    String imgUrl = "http://media.irib.ir/assets//radio_slider/20180819080845_7805.png";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_feom_web);

        img = findViewById(R.id.myImg);

        Glide.with(this).load(imgUrl).into(img);
    }
}
