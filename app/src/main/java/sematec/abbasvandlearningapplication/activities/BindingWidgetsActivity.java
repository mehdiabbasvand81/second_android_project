package sematec.abbasvandlearningapplication.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import sematec.abbasvandlearningapplication.R;

public class BindingWidgetsActivity extends AppCompatActivity
        implements View.OnClickListener {
    EditText fistName, lastName;
    Button register, signUp;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binding_widgets);
        //bind
        bind();
        //
        findViewById(R.id.register).setOnClickListener(this);
        findViewById(R.id.signup).setOnClickListener(this);

    }


    private void bind() {
        fistName = findViewById(R.id.name);
        lastName = findViewById(R.id.family);
        register = findViewById(R.id.register);
        result = findViewById(R.id.result);
        signUp = findViewById(R.id.signup);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.register) {
            String nameRes = fistName.getText().toString();
            String familyRes = lastName.getText().toString();
            result.setText(nameRes + " " + familyRes);
            fistName.setText("");
            lastName.setText("");
        } else if (view.getId() == R.id.signup) {
            Toast.makeText(BindingWidgetsActivity.this, "Hellow! This is a Toast.", Toast.LENGTH_LONG).show();
        }
    }
}