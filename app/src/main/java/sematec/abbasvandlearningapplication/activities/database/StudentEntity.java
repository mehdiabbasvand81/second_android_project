package sematec.abbasvandlearningapplication.activities.database;

import com.orm.SugarRecord;

public class StudentEntity extends SugarRecord<StudentEntity> {
    String userName, password, mobile;

    public StudentEntity(String userName, String password, String mobile) {
        this.userName = userName;
        this.password = password;
        this.mobile = mobile;
    }

    public StudentEntity() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
