package sematec.abbasvandlearningapplication.activities.database;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import sematec.abbasvandlearningapplication.R;

public class RejisterInDBActivity extends AppCompatActivity {
    EditText userName, password, mobile;
    DatabaseHandler db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejister_in_db);
        db = new DatabaseHandler(this, "sematec.db", null, 1);
        bind();
    }

    void bind() {
        userName = findViewById(R.id.username);
        password = findViewById(R.id.password);
        mobile = findViewById(R.id.mobile);

        findViewById(R.id.register).setOnClickListener(V -> {
//            db.addUser(userName.getText().toString(), password.getText().toString(), mobile.getText().toString());

            StudentEntity std = new StudentEntity();
            std.setMobile(mobile.getText().toString());
            std.setPassword(password.getText().toString());
            std.setUserName(userName.getText().toString());
            std.save();

            userName.setText("");
            password.setText("");
            mobile.setText("");
            loadData();
        });
        loadData();
    }

    void loadData() {
//        ((TextView) findViewById(R.id.resultshow)).setText(db.getUser());
        List<StudentEntity> students = StudentEntity.listAll(StudentEntity.class);
        ((TextView) findViewById(R.id.resultshow)).setText("");
        for (StudentEntity std : students) {
            ((TextView) findViewById(R.id.resultshow)).append(std.getUserName()
                    + " "
                    + std.getPassword()
                    + " "
                    + std.getMobile() + "\n");
        }
    }
}
