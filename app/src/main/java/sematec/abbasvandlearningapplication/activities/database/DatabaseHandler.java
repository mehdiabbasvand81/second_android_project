package sematec.abbasvandlearningapplication.activities.database;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class DatabaseHandler extends SQLiteOpenHelper {

    String stdTBLCreate = "" +
            " CREATE TABLE students ( " +
            " id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " username TEXT, " +
            " password TEXT, " +
            " mobile TEXT" +
            " )";

    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DatabaseHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version, @Nullable DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(stdTBLCreate);
    }

    public void addUser(String userName, String password, String mobile) {
        String insertQuery = "" +
                "INSERT INTO students(username, password, mobile)" +
                "VALUES ('" + userName + "','" + password + "','" + mobile + "')";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(insertQuery);
        db.close();
    }

    public String getUser() {
        String result = "";
        String selectQuery = "SELECT username, password, mobile FROM students";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        while (cursor.moveToNext()){
            result += cursor.getString(0)
                    +" "
                    +cursor.getString(1)
                    +" "
                    +cursor.getString(2)
                    +"\n";

        }
        return result;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
