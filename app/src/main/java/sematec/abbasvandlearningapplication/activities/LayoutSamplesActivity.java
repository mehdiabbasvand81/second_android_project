package sematec.abbasvandlearningapplication.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import sematec.abbasvandlearningapplication.R;

public class LayoutSamplesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_samples);
    }
}
