package sematec.abbasvandlearningapplication.activities;

import android.app.Application;

import com.orhanobut.hawk.Hawk;
import com.orm.SugarApp;

public class MyApplication extends SugarApp {

    @Override
    public void onCreate() {
        super.onCreate();
        Hawk.init(this).build();
    }
}
